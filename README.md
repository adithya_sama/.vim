to perform git commands with different key
    ssh-agent bash -c 'ssh-add /somewhere/yourkey; git clone git@github.com:user/project.git'

list submodules
    for x in $(find . -type d) ; do if [ -d "${x}/.git" ] ; then cd "${x}" ; origin="$(git config --get remote.origin.url)" ; cd - 1>/dev/null; echo git submodule add "${origin}" "${x}" ; fi ; done

add submodules
    for x in $(find . -type d) ; do if [ -d "${x}/.git" ] ; then cd "${x}" ; origin="$(git config --get remote.origin.url)" ; cd - 1>/dev/null; git submodule add "${origin}" "${x}" ; fi ; done

list submodule
    git submodule

update submodules
    cd into submodule, all the submodules are in bundle folder.
    git checkout <branch>.
    git coc.nvim the branch is release not master.
    the following command will update all the submodules to master.
        git submodule foreach git checkout master
    but we want coc.nvim to point to release not master. So that has to be done manually.


building vim:

    sudo make distclean

    ./configure --with-x

    sudo make

    sudo make install

