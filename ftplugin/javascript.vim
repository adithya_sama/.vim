" MAPPINGS
" --------
nnoremap <leader>l _iconsole.log(<esc>A);<esc>
nnoremap <leader>c 0i//<esc>
nnoremap <leader>C :s/^\( *\)\/\//\1/<CR>
vnoremap <leader>c :norm 0i//<CR>
vnoremap <leader>C :s/^\( *\)\/\//\1/<CR>
