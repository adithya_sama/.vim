" MAPPINGS
" --------
nnoremap <leader>c 0i--<esc>
nnoremap <leader>C :s/^\( *\)--/\1/<CR>
vnoremap <leader>c :norm 0i--<CR>
vnoremap <leader>C :s/^\( *\)--/\1/<CR>

setl formatprg=pg_format\ -t\ -g
