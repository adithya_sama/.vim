" INIT
" ===========================================================================================================
autocmd!

" DB TEXT
" ===========================================================================================================

let g:dbext_default_profile_psql = 'type=PGSQL:host=localhost:port=5432:dbname=acaldb:user=acalvio'
let g:dbext_default_profile = 'psql'
let g:omni_sql_no_default_maps = 1
"autocmd VimEnter * DBCompleteTables


" TEMP
" ===========================================================================================================

" Disable Arrow keys in Normal mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
"
" " Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" GAMEPLAY
" ===========================================================================================================

set t_Co=256
set number
set tabstop=4
set shiftwidth=4
syntax on
set backspace=indent,eol,start
set autoindent
set statusline+=%F
set spelllang=en
set spell
set expandtab
set splitbelow
set splitright
set esckeys
hi SpellBad cterm=underline ctermbg=16
filetype indent on
set ff=unix
set rtp+=~/.fzf
set complete+=k./*
set cursorcolumn
set cursorline
" updatetime controls when vim writes to its swap files.
set updatetime=100
set scrolloff=10 "keeps cursor in the middle of the screen

" MAPPINGS
" ===========================================================================================================

let mapleader = "-"

" NETRW
" ===========================================================================================================

let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 20
let g:netrw_altv = 1
let g:netrw_browse_split = 0

nnoremap <leader>d :Lex .<CR><CR>

"augroup netrw
"    autocmd!
"    autocmd BufLeave NetrwTreeListing* :bd
"augroup END;

" BUFFERS
" ===========================================================================================================

nnoremap ` :bn<CR>
nnoremap ~ :bp<CR>
nnoremap <leader>q :bd<CR>

" AIRLINE
" ===========================================================================================================

let g:airline_theme='angr'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#hunks#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#formatter = 'short_path'
let g:airline_powerline_fonts = 1

" ALE
" ===========================================================================================================

"let g:ale_completion_enabled = 1
"let g:ale_sign_column_always = 1
"set completeopt=menu,menuone,preview,noinsert
"inoremap <silent><expr> <Tab> pumvisible() ? "\<C-n>" : "\<TAB>"
"nnoremap <leader>se :ALEToggle<CR>
"nnoremap <leader>e :lnext<CR>
"nnoremap <leader>E :lprev<CR>
"let g:ale_linters = {'python': ['pylint', 'pyls'],}
"let b:ale_fixers = ['trim_whitespace']

" POLYGLOT
" ===========================================================================================================

let g:sql_type_default = 'pgsql'

" PRETTIER
" ===========================================================================================================

let g:prettier#exec_cmd_async = 1
vnoremap <leader>p :PrettierPartial<cr>

" POPUP MAPPINGS
" ===========================================================================================================
inoremap <expr> `           pumvisible() ? "\<C-p>" : "`"
inoremap <expr> <Up>        pumvisible() ? "\<C-y><Up>" : "\<Up>"
inoremap <expr> <Down>      pumvisible() ? "\<C-y><Down>" : "\<Down>"

" AUTO-PAIR
" ===========================================================================================================

" FOLDING
" ===========================================================================================================

" INDENTATION CONFIG
" ===========================================================================================================

let g:indentLine_color_term = 239
let g:indentLine_char = '▏'
" let g:indentLine_faster = 1
set conceallevel=1

" VIM  OVERRIDES
" ===========================================================================================================

" keeps the cursor on the middle of the screen on search 
nnoremap <expr>n 'nzz'
nnoremap <expr>N 'Nzz'

" keeps the cursor on the middle of the screen on moving to mark 
noremap <expr>' "'" . nr2char(getchar()) . 'zz'

" CTRL P
" ===========================================================================================================

nnoremap <leader>b :CtrlPBuffer<CR>
nnoremap <leader>e :CtrlPMixed<CR>


" PGSQL.VIM
" ===========================================================================================================
let g:sql_type_default = 'pgsql'

" COC
" ===========================================================================================================

let g:coc_global_extensions = [
    \ 'coc-eslint',
    \ 'coc-sql',
    \ 'coc-css',
    \ 'coc-html',
    \ 'coc-json',
    \ 'coc-jedi',
    \ 'coc-tsserver'
    \ ]

let g:coc_disable_startup_warning = 1

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
"inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
"if exists('*complete_info')
"  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
"else
"  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
" nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" VIM Multi Cursor
" ===========================================================================================================

let g:VM_no_meta_mappings = 1
let g:VM_maps = {}
let g:VM_maps["Add Cursor Down"]   = '<C-j>'
let g:VM_maps["Add Cursor Up"]     = '<C-k>'
let g:VM_Mono_hl   = 'DiffText'
let g:VM_Extend_hl = 'DiffAdd'
let g:VM_Cursor_hl = 'Visual'
let g:VM_Insert_hl = 'DiffChange'

" GENERAL  MAPPINGS
" ===========================================================================================================

" making search not exact by default
nnoremap * g*
nnoremap # g#

" upper case.
nnoremap <leader>u viwUw

" correct spelling.
nnoremap <leader>z 1z=

" encapsulate in quotes
nnoremap <leader>" bi"<esc>lviw<esc>a"<esc>
nnoremap <leader>' bi'<esc>lviw<esc>a'<esc>

" formatting sql
vnoremap <leader>iq :!sqlformat --reindent --keywords upper --identifiers lower -<CR>
vnoremap <leader>ij :!jq --indent 4 '.'<CR>

" move to next/prev wrong spelling (respectively).
nnoremap ]n ]s
nnoremap [n [s

" copy to clipboard
vnoremap <leader>y "+y

augroup _workarounds
    autocmd!
    " copy clipboard value to xclip to retain clipboard on exit.
    autocmd VimLeave * exe ":!echo " . shellescape(getreg('+')) . " | xclip -selection clipboard"
augroup END

" open todo file
:command Todo e ~/info/todo.txt
" open pr file
:command Info e /repos/info.txt

" THEMEING
" ===========================================================================================================

colo birds-of-paradise
hi CursorLine ctermbg=233
hi CursorColumn ctermbg=233
hi CurrentWordTwins ctermbg=238 cterm=none
hi CurrentWord ctermbg=238 cterm=none
hi SpellBad cterm=underline ctermbg=none
hi SpellCap cterm=underline ctermbg=none
hi LineNr ctermbg=232 ctermfg=245
hi visual ctermbg=240
"hi clear SignColumn
"hi ALEErrorSign ctermbg=232 ctermfg=160
"hi ALEWarningSign ctermbg=232 ctermfg=11
"let g:ale_sign_error = '>>'
"let g:ale_sign_warning = '--'
"hi Folded ctermfg=232 ctermbg=66

" PATHOGEN
" ===========================================================================================================

execute pathogen#infect()
set nocompatible
filetype plugin indent on

" PLUGIN  DOCUMENTATION  RELATED
" ===========================================================================================================
"
" Put these lines at the very end of your vimrc file.
"
" " Load all plugins now.
" " Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" " Load all of the helptags now, after plugins have been loaded.
" " All messages and errors will be ignored.
silent! helptags ALL

" TEST
" ===========================================================================================================

function! OpenURLUnderCursor()
  let s:uri = expand('<cWORD>')
  let s:uri = substitute(s:uri, '?', '\\?', '')
  let s:uri = shellescape(s:uri, 1)
  if s:uri != ''
    silent exec "!open '".s:uri."'"
    :redraw!
  endif
endfunction
nnoremap gx :call OpenURLUnderCursor()<CR>
