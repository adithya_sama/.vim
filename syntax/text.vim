syn match checkbox_not_done '\[ \]'
hi checkbox_not_done term=bold ctermfg=DarkRed guifg=#80a0ff gui=bold

syn match checkbox_done '\[x\]'
hi checkbox_done term=bold ctermfg=DarkGreen guifg=#80a0ff gui=bold
